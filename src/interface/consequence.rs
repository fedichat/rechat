#[derive(Debug)]
pub enum Consequence {
    Message {
        author: String,
        body: String,
    },
}
