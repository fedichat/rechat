use std::fmt;
use std::mem;
use std::str::FromStr;

const COMMAND_CHAR: char = '/';
const ESCAPE_CHAR: char = '\\';
const SPECIAL_CHARS: [char; 3] = ['"', '\\', ' '];

#[derive(Clone, Debug)]
pub enum Action {
    Message(String),
    SignIn {
        host: String,
        name: String,
        pass: String,
    },
    Quit,
}

impl FromStr for Action {
    type Err = InputError;

    fn from_str(line: &str) -> Result<Self, Self::Err> {
        let first_char = line.chars().next();

        let command = if line.is_empty() {
            return Err(InputError::Empty);
        } else if first_char != Some(COMMAND_CHAR) {
            // If it's not a command, make it a command
            escape(&line, Some("/say "))
        } else {
            if line.len() == 1 && first_char == Some(COMMAND_CHAR) {
                return Err(InputError::Empty);
            }

            // Use as-is
            String::from(line)
        };

        // Tokenize the command, omitting the leading `/`
        let mut tokenized = tokenize(&command[1..command.len()])?;

        // Convert stringly-typed tokens to a strongly-typed enum
        match tokenized[0].as_ref() {
            "quit" => Ok(Action::Quit),
            "say" => Ok(Action::Message(tokenized.remove(1))),
            _ => Err(InputError::UnknownCommand(tokenized.remove(0))),
        }
    }
}

#[derive(Debug)]
pub enum InputError {
    Empty,
    EscapeEol,
    MismatchedQuotes,
    UnknownCommand(String),
    UnknownEscape(char),
}

impl fmt::Display for InputError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Empty => write!(f, "input is empty"),

            Self::EscapeEol => write!(f, "escape character at end-of-line"),

            Self::MismatchedQuotes => write!(f, "mismatching quotes"),

            Self::UnknownCommand(cmd) => {
                write!(f, "unknown command \"{}\"", cmd)
            }

            Self::UnknownEscape(c) => {
                write!(f, "unknown escape \"{}{}\"", ESCAPE_CHAR, c)
            }
        }
    }
}

fn tokenize(line: &str) -> Result<Vec<String>, InputError> {
    let mut in_quote = false;
    let mut chars = line.chars();
    let mut token = String::new();
    let mut tokens = Vec::new();

    // Push a complete token to the list of tokens
    let mut end_token = |token: &mut String| {
        // Empty tokens are passed if there are multiple unescaped whitespaces
        // in a row, so filter those out
        if !token.is_empty() {
            let token = mem::replace(token, String::new());
            tokens.push(token);
        }
    };

    loop {
        match chars.next() {
            Some('"') => {
                // At the second quote in a quote pair, finalize the preceding
                // token
                if in_quote {
                    end_token(&mut token);
                }

                // At the first quote in a quote pair, stop treating whitespace
                // as a token separator (logic for that is below)
                in_quote = !in_quote;
            }

            // The next character should be an escaped special character
            Some(ESCAPE_CHAR) => {
                // Attempt to acquire the literal special character
                match chars.next() {
                    // The escaped character is a known special character, add
                    // the special character without the escape character to the
                    // current token
                    Some(c) if SPECIAL_CHARS.contains(&c) => token.push(c),

                    // The escaped character has no special meaning and doesn't
                    // need to be escaped
                    Some(c) => return Err(InputError::UnknownEscape(c)),

                    // The escape character appeared at the end of the line,
                    // escaping nothing
                    None => return Err(InputError::EscapeEol),
                }
            }

            // `c` is unquoted whitespace, finalize the preceding token
            Some(c) if c.is_whitespace() && !in_quote => end_token(&mut token),

            // `c` is nothing special, add it to the current token
            Some(c) => token.push(c),

            // When the end of the line is reached, finalize the preceding token
            None => {
                end_token(&mut token);
                break;
            }
        }
    }

    if in_quote {
        // If the end of the line is reached but a quote is still open, throw an
        // error
        Err(InputError::MismatchedQuotes)
    } else {
        // If everything matches up and tokenizes nicely, return the tokens
        Ok(tokens)
    }
}

// Escape special characters, optionally prepend something to the new string
fn escape(input: &str, prepend: Option<&str>) -> String {
    let mut escaped = if let Some(p) = prepend {
        String::from(p)
    } else {
        String::new()
    };

    for c in input.chars() {
        if SPECIAL_CHARS.contains(&c) {
            escaped.push(ESCAPE_CHAR);
        }
        escaped.push(c);
    }

    escaped
}
