use std::sync::mpsc;

use cursive::{
    align::HAlign,
    event::{Callback, Event, EventResult, EventTrigger, Key},
    theme::{BorderStyle, Color, Palette, PaletteColor, Theme},
    view::{Boxable, Identifiable, ScrollStrategy},
    views::{
        Dialog, DummyView, EditView, LinearLayout, NamedView, OnEventView,
        ScrollView, TextArea, TextView,
    },
    Cursive, CursiveExt,
};

use crate::interface::{action::InputError, Action, Consequence};

pub struct Ui {
    siv: Cursive,

    // Poll this for new content
    consequence_rx: mpsc::Receiver<Consequence>,

    // Send user actions to the backend through here
    actions_tx: mpsc::Sender<Action>,
}

impl Ui {
    /// Create a new UI
    ///
    /// * **consequence_rx** receives events that should be displayed by the UI
    ///   somehow.
    /// * **actions_tx** sends events caused by interactions with the UI.
    pub fn new(
        consequence_rx: mpsc::Receiver<Consequence>,
        actions_tx: mpsc::Sender<Action>,
    ) -> Self {
        let mut new = Self {
            siv: Cursive::default(),
            consequence_rx,
            actions_tx,
        };

        new.apply_theme();
        new.apply_sign_in_layout();
        new
    }

    fn apply_theme(&mut self) {
        let mut palette = Palette::default();
        palette[PaletteColor::Background] = Color::TerminalDefault;
        palette[PaletteColor::Shadow] = Color::TerminalDefault;
        palette[PaletteColor::View] = Color::TerminalDefault;
        palette[PaletteColor::Primary] = Color::TerminalDefault;
        palette[PaletteColor::Secondary] = Color::TerminalDefault;

        let theme = Theme {
            shadow: false,
            borders: BorderStyle::None,
            palette,
        };

        self.siv.set_theme(theme);
    }

    fn apply_sign_in_layout(&mut self) {
        let actions_tx = self.actions_tx.clone();

        let sign_in =
            Dialog::new().title("Sign In").padding_lrtb(1, 1, 1, 0).content(
                LinearLayout::vertical()
                    .child(
                        TextView::new("Server Address").h_align(HAlign::Center),
                    )
                    .child(
                        EditView::new()
                            .on_submit(|s, _| s.focus_name("name").unwrap())
                            .with_name("host")
                            .fixed_width(20),
                    )
                    .child(DummyView)
                    .child(TextView::new("Username").h_align(HAlign::Center))
                    .child(
                        EditView::new()
                            .on_submit(|s, _| s.focus_name("pass").unwrap())
                            .with_name("name")
                            .fixed_width(20),
                    )
                    .child(DummyView)
                    .child(TextView::new("Password").h_align(HAlign::Center))
                    .child(
                        EditView::new()
                            .on_submit(move |s, _| {
                                Self::sign_in(s, actions_tx.clone())
                            })
                            .secret()
                            .with_name("pass")
                            .fixed_width(20),
                    ),
            );

        self.siv.add_layer(sign_in);
    }

    fn sign_in(siv: &mut Cursive, actions_tx: mpsc::Sender<Action>) {
        let host = siv
            .call_on_name("host", |v: &mut EditView| (*v.get_content()).clone())
            .unwrap();

        let name = siv
            .call_on_name("name", |v: &mut EditView| (*v.get_content()).clone())
            .unwrap();

        let pass = siv
            .call_on_name("pass", |v: &mut EditView| (*v.get_content()).clone())
            .unwrap();

        if !host.is_empty() && !name.is_empty() && !pass.is_empty() {
            actions_tx
                .send(Action::SignIn {
                    host,
                    name,
                    pass,
                })
                .unwrap();

            siv.pop_layer();
            Self::apply_main_layout(siv, actions_tx);
        } else {
            let error_msg = Dialog::new()
                .title("Error")
                .padding_lrtb(1, 1, 1, 0)
                .content(TextView::new(
                    "Server Address, Username, and Password cannot be blank!",
                ))
                .button("Accept", |s| {
                    s.pop_layer().unwrap();
                });

            siv.add_layer(error_msg);
        }
    }

    fn apply_main_layout(siv: &mut Cursive, actions_tx: mpsc::Sender<Action>) {
        let message_log =
            ScrollView::new(TextView::empty().with_name("message_log"))
                .scroll_strategy(ScrollStrategy::StickToBottom)
                .with_name("scroller")
                .full_screen();

        let command_line = OnEventView::new(TextArea::new())
            .on_pre_event_inner(
                EventTrigger::any(),
                move |text_area, event| {
                    Self::process_text_area_input(
                        text_area,
                        event,
                        actions_tx.clone(),
                    )
                },
            );

        siv.add_fullscreen_layer(
            LinearLayout::vertical().child(message_log).child(command_line),
        );
    }

    fn process_text_area_input(
        text_area: &mut TextArea,
        event: &Event,
        actions_tx: mpsc::Sender<Action>,
    ) -> Option<EventResult> {
        if event != &Event::Key(Key::Enter) {
            return None;
        }

        let callback = text_area
            .get_content()
            .parse::<Action>()
            .map(|action| {
                actions_tx.send(action.clone()).unwrap();

                match action {
                    Action::Quit => {
                        // Shut down Cursive
                        Some(Callback::from_fn(|siv: &mut Cursive| {
                            siv.quit();
                        }))
                    }

                    _ => None,
                }
            })
            .unwrap_or_else(|e| match e {
                // Ignore this error
                InputError::Empty => None,

                // Log others
                x => {
                    tracing::warn!("command error: {}", x);
                    None
                }
            });

        text_area.set_content("");
        Some(EventResult::Consumed(callback))
    }

    /// Run the TUI
    ///
    /// This blocks until the user exits it, so spawning this in a thread is
    /// recommended.
    pub fn run(mut self) {
        // Not sure why this makes it work but it does, so there you go
        self.siv.refresh();

        // Custom cursive loop
        loop {
            if self.siv.is_running() {
                // Run cursive
                self.siv.step();

                // If the user scrolled away from the bottom and came back,
                // reset the scroll strategy to stay at the bottom
                self.siv.call_on_name(
                    "scroller",
                    |view: &mut ScrollView<NamedView<TextView>>| {
                        if view.is_at_bottom() {
                            view.set_scroll_strategy(
                                ScrollStrategy::StickToBottom,
                            );
                        }
                    },
                );

                // Use try_recv instead of recv because otherwise the loop would
                // block on new content and disallow the user from interacting
                // with the program before it arrives. We want to be able to
                // send our own content, so we mustn't block here.
                match self.consequence_rx.try_recv() {
                    Ok(consequence) => {
                        tracing::info!(
                            "received consequence: {:?}",
                            consequence
                        );
                        // Add new content to screen
                        self.siv.call_on_name(
                            "message_log",
                            |view: &mut TextView| match consequence {
                                Consequence::Message {
                                    author,
                                    body,
                                } => {
                                    view.append(format!(
                                        "{}: {}\n",
                                        author, body
                                    ));
                                }
                            },
                        );

                        // Manually refresh screen to show new content
                        self.siv.refresh();
                    }

                    // If there's nothing new, that's fine
                    Err(e) if e == mpsc::TryRecvError::Empty => (),

                    // If it's disconnected, it's probably because the program
                    // is shutting down. We'll log it just in case but it's
                    // unlikely to be a real problem. I could have added an
                    // AtomicBool somewhere to indicate shutdown and test that
                    // but that's work I don't feel like doing right now.
                    Err(_) => {
                        tracing::warn!(
                            "ui sender(s) dropped; this probably \
                              occurred during shutdown, which is normal"
                        );
                        break;
                    }
                }
            } else {
                break;
            }
        }
    }
}
