use std::{sync::mpsc, thread};

mod interface;
mod logger;

use interface::{Action, Consequence, Ui};

fn main() {
    logger::init();

    let (actions_tx, actions_rx) = mpsc::channel();
    let (in_tx, in_rx) = mpsc::channel();

    let backend = thread::spawn(move || {
        let span = tracing::trace_span!("backend");
        let _enter = span.enter();

        let mut current_name: Option<String> = None;

        for action in actions_rx.iter() {
            tracing::info!("received action: {:?}", action);

            match action {
                // Replace this with getting stuff over the network instead of
                // local echo
                Action::Message(body) => {
                    let c = Consequence::Message {
                        author: current_name.clone().unwrap(),
                        body,
                    };

                    in_tx.send(c).unwrap();
                }
                Action::SignIn {
                    name,
                    ..
                } => current_name = Some(name),
                Action::Quit => return,
            }
        }
    });

    let span = tracing::trace_span!("ui");
    let _enter = span.enter();
    Ui::new(in_rx, actions_tx).run();

    backend.join().unwrap();
}
