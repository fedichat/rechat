use std::io::stderr;

use tracing::Level;
use tracing_subscriber::FmtSubscriber;

/// Initialize the logger
pub fn init() {
    let sub = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .with_writer(stderr)
        .finish();

    tracing::subscriber::set_global_default(sub)
        .expect("setting default subscriber failed");
}
