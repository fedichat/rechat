#!/bin/bash

set -e

# Run auto-formatter
cargo fmt

# Get a list of new files in the index
ADDED=$(git diff --name-only --staged --diff-filter=d)

# Re-add changed files
git add $ADDED

# Deny warnings
cargo clippy -- -Dwarnings

# Run tests
[ -z "${SKIP_TESTS_PLEASE}" ] && cargo test || echo "Testing skipped."
