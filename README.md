# ReChat

TUI client for the Fedichat protocol

---

Well, it will be eventually.

## Running

I recommend piping stderr to a file and then `tail -f`ing that file. If you
don't do the former, the TUI will get garbled with log outputs, and the latter
lets you view the log as in real time.

## Contributing

Be sure to run the `.hooks/install.sh` script after the first clone. The
post-merge hook will re-run this script to apply changes, if there are any. The
pre-commit hook runs `cargo fmt` and `cargo test`. If you don't want to run the
tests for some reason (maybe you already did and just don't want to do it again
automatically), you can set the `SKIP_TESTS_PLEASE` environment variable to
anything.

## License

This project is licensed under either of

* Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  <http://www.apache.org/licenses/LICENSE-2.0>)

* MIT license ([LICENSE-MIT](LICENSE-MIT) or
  <http://opensource.org/licenses/MIT>)

at your option.
